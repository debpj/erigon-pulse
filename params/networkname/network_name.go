package networkname

const (
	MainnetChainName             = "mainnet"
	SepoliaChainName             = "sepolia"
	RinkebyChainName             = "rinkeby"
	GoerliChainName              = "goerli"
	DevChainName                 = "dev"
	SokolChainName               = "sokol"
	BSCChainName                 = "bsc"
	ChapelChainName              = "chapel"
	RialtoChainName              = "rialto"
	MumbaiChainName              = "mumbai"
	BorMainnetChainName          = "bor-mainnet"
	BorDevnetChainName           = "bor-devnet"
	GnosisChainName              = "gnosis"
	ChiadoChainName              = "chiado"
	PulsechainChainName          = "pulsechain"
	PulsechainDevnetChainName    = "pulsechain-devnet"
	PulsechainTestnetV3ChainName = "pulsechain-testnet-v3"
)

var All = []string{
	MainnetChainName,
	SepoliaChainName,
	RinkebyChainName,
	GoerliChainName,
	SokolChainName,
	BSCChainName,
	ChapelChainName,
	//RialtoChainName,
	MumbaiChainName,
	BorMainnetChainName,
	BorDevnetChainName,
	GnosisChainName,
	ChiadoChainName,
	PulsechainChainName,
	PulsechainDevnetChainName,
	PulsechainTestnetV3ChainName,
}
